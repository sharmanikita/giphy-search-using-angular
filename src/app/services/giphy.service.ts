import {Injectable} from '@angular/core';
import {GiphySearchInput} from '../search/giphy-search-input';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GiphyService {

  giphySearchInput: GiphySearchInput = new GiphySearchInput();

  private url = '';
  public results;
  public giphyState: Subject<any> = new Subject();
  public giphy$ = this.giphyState.asObservable();

  constructor(private http: HttpClient) {
  }

  loadGiphys(searchInput) {
    this.url = `http://api.giphy.com/v1/gifs/search?q=${searchInput}&api_key=sxSSTvCnao4Txb3zG4Bm2KzzMXhdrPaO`;
    return this.http.get(`${this.url}`);
  }

  public setResults(results) {
    this.results = results;
    this.giphyState.next({
      data: results
    });
  }
}
