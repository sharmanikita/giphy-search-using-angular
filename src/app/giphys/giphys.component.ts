import { Component, OnInit } from '@angular/core';
import {GiphyService} from '../services/giphy.service';
import {GiphySearchInput} from '../search/giphy-search-input';

@Component({
  selector: 'app-giphys',
  templateUrl: './giphys.component.html',
  styleUrls: ['./giphys.component.css']
})
export class GiphysComponent implements OnInit {

  constructor(private giphyService: GiphyService) {
  }

  giphySearchInput: GiphySearchInput = new GiphySearchInput();
  results: any;

  private giphyData: any;

  ngOnInit() {
    this.giphyData = this.giphyService.giphy$.subscribe(results => {
      this.results = results.data;
    });
  }

}
