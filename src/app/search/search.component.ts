import {Component, OnInit} from '@angular/core';
import {GiphyService} from '../services/giphy.service';
import {GiphySearchInput} from './giphy-search-input';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private giphyService: GiphyService) {
  }

  giphySearchInput: GiphySearchInput = new GiphySearchInput();

  ngOnInit() {
  }

  search() {
    return this.giphyService.loadGiphys(this.giphySearchInput.giphySearchInput).pipe(take(1)).subscribe((res: any) => {

      this.giphyService.setResults(res.data);
      this.giphySearchInput.giphySearchInput = '';
    });
  }

}
